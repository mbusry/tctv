//
//  CollectionViewCollectionViewCell.swift
//  TCTV
//
//  Created by Michael on 11/21/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit

class CollectionViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var serviceImageIcon: UIImageView!
    
    
}
