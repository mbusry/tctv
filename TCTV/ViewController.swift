//
//  ViewController.swift
//  TCTV
//
//  Created by Michael on 11/14/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import WebKit

//@IBOutlet var collectionView: [UICollectionView]!

//@IBOutlet weak var collectionView: UICollectionView!
//@IBOutlet weak var mainCollectionView: UICollectionView!
//@IBOutlet weak var collectionView: UICollectionView!

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIWebViewDelegate {
    
    
    let clips = ["http://video.wixstatic.com/video/6726b3_05f3433ddf5542df89e60106a58a2456/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_0f583ca99a8e47c698682be776ba9e34/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_87e221efe1fc4579aa1f4a509894f6dd/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_59b53787528e467a801289f449ccdd52/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_c1a82bb2799043aea1e098a4c4e45f4e/720p/mp4/file.mp4"]
    
    let clipsInfo = ["Give Thanks Pt1","Give Thanks Pt2","Give Thanks Pt3","Give Thanks Pt4","Give Thanks Pt5"]
    
    var selectedRow:Int!
    var selectedURL:URL!
    var isPlayButtonPressed:Bool?
    
    var externalSite:String!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet var collectionView: [UICollectionView]!
    
    
    var avPlayer:AVPlayer?
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var TcInfoLabel: UILabel!
    
    let campus:NSURL? = NSURL(string:"http://www.trinitychapel.org")
    let fb:NSURL? = NSURL(string:"https://www.facebook.com/tcbroadcast")
    let twit:NSURL? = NSURL(string:"https://twitter.com/tc_live")
    let flick:NSURL? = NSURL(string:"http://www.flickr.com/photos/tcbroadcast")
    let vim:NSURL? = NSURL(string:"https://vimeo.com/trinitychapel")
    
    let genericIcon:NSURL? = NSURL(string:"http://visioncastmedia.com/trinitychapel/mediacenter/wp-content/uploads/2011/02/tcmediacenter-portfolioimage11-256x170.jpg")
//    @IBOutlet weak var collectionView: UICollectionView!
    
    let videoClip0URL:NSURL? = NSURL(string:"http://video.wixstatic.com/video/6726b3_05f3433ddf5542df89e60106a58a2456/720p/mp4/file.mp4")
    
    let videoClip1URL:NSURL? = NSURL(string:"http://video.wixstatic.com/video/6726b3_0f583ca99a8e47c698682be776ba9e34/720p/mp4/file.mp4")
    
    let videoClip2URL:NSURL? = NSURL(string:"http://video.wixstatic.com/video/6726b3_87e221efe1fc4579aa1f4a509894f6dd/720p/mp4/file.mp4")

    let videoClip3URL:NSURL? = NSURL(string:"http://video.wixstatic.com/video/6726b3_59b53787528e467a801289f449ccdd52/720p/mp4/file.mp4")

    let videoClip4URL:NSURL? = NSURL(string:"http://video.wixstatic.com/video/6726b3_c1a82bb2799043aea1e098a4c4e45f4e/720p/mp4/file.mp4")
    
    let liveStreamURL:NSURL? = NSURL(string:"http://wowzaprodhd50-lh.akamaihd.net/i/f1e9178f_1@348349/master.m3u8")
    let sunday1URL:NSURL? = NSURL(string:"https://player.vimeo.com/video/191375435")
    let sunday2URL:NSURL? = NSURL(string:"https://player.vimeo.com/video/190461981")
    let wednesday1URL:NSURL? = NSURL(string:"https://player.vimeo.com/video/190104294")
    
    let testVideoURL:NSURL? = NSURL(string:"http://devstreaming.apple.com/videos/wwdc/2016/102w0bsn0ge83qfv7za/102/hls_vod_mvp.m3u8")

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.clips.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CollectionViewCollectionViewCell
        
        cell.myLabel.text = self.clipsInfo[indexPath.item]
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: self.genericIcon as! URL)
            DispatchQueue.main.async {
                cell.serviceImageIcon.image = UIImage(data: data!)
            }
        }

        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NSLog("You selected %i",indexPath.item)
        // videoClip4URL
        
        selectedRow = indexPath.item
        
        let sr = String(selectedRow)
        
        let st1 = "videoClip"
        let st2 = "URL"
        let combined:String = st1 + sr + st2

        selectedURL = NSURL(string:clips[selectedRow]) as URL!
        
        NSLog("You selected URL %@",combined)
        NSLog("You selected URL %@",clips[selectedRow])

        
        performSegue(withIdentifier: "AVPlayerSegue", sender: self)


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func onClick(sender: UIButton) {
        
        switch sender.tag
        {
        case 0:
            
            isPlayButtonPressed = true
            
            selectedURL = liveStreamURL as URL!
            
            performSegue(withIdentifier: "AVPlayerSegue", sender: self)

            break;
            
        case 1:
            
            UIApplication.shared.open(fb as! URL, options: [:], completionHandler:nil)
            
            break;
            
        case 2:
            
            UIApplication.shared.open(twit as! URL, options: [:], completionHandler:nil)
            
            break;
            
        case 3:
            
            UIApplication.shared.open(flick as! URL, options: [:], completionHandler:nil)

            break;
            
        case 4:
            
            UIApplication.shared.open(vim as! URL, options: [:], completionHandler:nil)

            break;
            
        case 5:
            
            
            UIApplication.shared.open(campus as! URL, options: [:], completionHandler:nil)

            break;
            
            
        default: ()
        break;
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
                

        
        if segue.identifier == "AVPlayerSegue"{
            
            if isPlayButtonPressed! {
            
            let destination = segue.destination as! AVPlayerViewController
            
            destination.player = AVPlayer(url: selectedURL as URL)
            destination.player?.play()
            destination.player?.volume = 1
            }
            
            isPlayButtonPressed = false
            
        }
    }
    
}
