//
//  webViewViewController.swift
//  TCTV
//
//  Created by Michael on 11/30/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    var urlWebsite: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let myURLString = urlWebsite
        let myURL = NSURL(string: myURLString!)
        let myURLRequest = NSURLRequest(url: myURL! as URL)
        webView.loadRequest(myURLRequest as URLRequest)

    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        

    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
