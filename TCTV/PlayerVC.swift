//
//  AvPlayerViewController.swift
//  TCTV
//
//  Created by Michael on 11/17/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

class PlayerVC: UIViewController {
    
    let avPlayerVC = AVPlayerViewController()
    var avPlayer:AVPlayer?
    
    var videoURL:NSURL?


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NSLog("videoURL %@",videoURL!)
        
        if let url = videoURL {
            self.avPlayer = AVPlayer(url: url as URL)
            self.avPlayerVC.player = self.avPlayer
            
            
            
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
