//
//  ViewController.swift
//  TCTVtv
//
//  Created by Michael on 12/1/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit
import AVKit


class ViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    var sermonIconURL:String!
    var isJsonLoaded = Bool()
    var sermons = [[String: String]]()
    var service1StartTime:String!
    var service2StartTime:String!
    var wednesdayStartTime:String!
    var tcInfo:String = String("")
    var passIconUrl:NSURL!
    var headerImageUrl = String("")
    var backgroundImageUrl = String("")
    var videoImageURL = String("")
    var isPlayButtonPressed:Bool?

    var avPlayer:AVPlayer?
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var tcInfoLabel: UILabel!
    @IBOutlet weak var startStreamingButton: UIButton!
    
    //    let url = NSURL(string: "https://quarkbackend.com/getfile/mbudesigns/trinitychapel-json")
    let jsonURL = String("https://quarkbackend.com/getfile/mbudesigns/trinitychapel-json")
    let defaultTcInfo = String("We are a diverse and loving church, passionately sharing the life-changing power of Jesus Christ with others. This vision is built upon three fundamental values: love, acceptance, and forgiveness. These values were evident in the life of Jesus Christ and we want them to be just as evident in our individual lives.")
    
    let clips = ["http://video.wixstatic.com/video/6726b3_05f3433ddf5542df89e60106a58a2456/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_0f583ca99a8e47c698682be776ba9e34/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_87e221efe1fc4579aa1f4a509894f6dd/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_59b53787528e467a801289f449ccdd52/720p/mp4/file.mp4","http://video.wixstatic.com/video/6726b3_c1a82bb2799043aea1e098a4c4e45f4e/720p/mp4/file.mp4"]
    
    var selectedRow:Int!
    var selectedURL:URL!
    var externalSite:String!
    var passSermonInfo:String!
    var passSermonTitle:String!
    
    
    let liveStreamURL:NSURL? = NSURL(string:"http://wowzaprodhd50-lh.akamaihd.net/i/f1e9178f_1@348349/master.m3u8")
    
    let genericIconURL:NSURL? = NSURL(string:"http://visioncastmedia.com/trinitychapel/mediacenter/wp-content/uploads/2011/02/tcmediacenter-portfolioimage11-256x170.jpg")
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        //        downloadData()
        getJson()
        getCurrentDate()
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.startStreamingButton.setImage(UIImage(named: "MediaStreamingLogo"), for: UIControlState.normal)
        
        self.startStreamingButton.setBackgroundImage(UIImage(named: "MediaStreamingLogo"), for: UIControlState.normal)
        
        if (headerImageUrl?.isEmpty)!{
            headerImageView.image = UIImage(named:"TClogo")
        }
        
        if  (backgroundImageUrl != nil){
            
        }
        
        
        if (tcInfo.isEmpty) {
            self.tcInfoLabel.text = defaultTcInfo
        }else{
            self.tcInfoLabel.text = tcInfo
        }
        
        
    }
    
    func parse(json: JSON) {
        
        for result in json["sermons"].arrayValue {
            let title = result["sermon-title"].stringValue
            let speaker = result["sermon-speaker"].stringValue
            let date = result["sermon-date"].stringValue
            let desc = result["sermon-description"].stringValue
            let su = result["sermon-url"].stringValue
            let siu = result["sermon-icon-url"].stringValue
            let iv = result["video-info-imageview"].stringValue
            
            let obj = ["sermon-title": title,
                       "sermon-speaker": speaker,
                       "sermon-date":date,
                       "sermon-description":desc,
                       "sermon-url":su,
                       "sermon-icon-url":siu,
                       "video-info-imageview":iv]
            sermons.append(obj)
        }
        
        collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClick(sender: UIButton) {
        
        switch sender.tag
        {
        case 0:
            
            
            selectedURL = liveStreamURL as URL!
            
            isPlayButtonPressed = true
            
            performSegue(withIdentifier: "LiveStreamSegue", sender: self)
            
            break;
            
        default: ()
        break;
            
        }
    }
    
    @nonobjc func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.sermons.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CollectionViewCell
        let sermon = sermons[indexPath.row]
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.myLabel.text = sermon["sermon-title"]
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: self.genericIconURL as! URL)
            DispatchQueue.main.async {
                cell.serviceImageView.image = UIImage(data: data!)
            }
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NSLog("You selected %i",indexPath.item)
        
        selectedRow = indexPath.item
        
        let sermon = sermons[selectedRow]

        
        selectedURL = NSURL(string:sermon["sermon-url"]!) as URL!
        
        passSermonInfo = sermon["sermon-description"]
        
        passSermonTitle = sermon["sermon-title"]
        
        let iconUrl = sermon["sermon-icon-url"]

        if (iconUrl?.isEmpty)! {
            passIconUrl = genericIconURL!
        }else{
            passIconUrl = NSURL(string:sermon["sermon-icon-url"]!)!
        }

        
        
        performSegue(withIdentifier: "VideoInfoSegue", sender: self)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        
      
        if segue.identifier == "LiveStreamSegue"{
            if isPlayButtonPressed! {
             
            
            let destination = segue.destination as! AVPlayerViewController
            
            destination.player = AVPlayer(url: selectedURL as URL)
            destination.player?.play()
            destination.player?.volume = 1
            }
            
            isPlayButtonPressed = false
        }

        
        if segue.identifier == "VideoInfoSegue"{
            
            let destination = segue.destination as! VideoInfoViewController
            
            destination.buttonImageURL = passIconUrl
            
            destination.videoInfoText = passSermonInfo as NSString?
            
            destination.sermonTitleText = passSermonTitle as NSString?
            
            destination.selectedURL = selectedURL
            
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionView, minimumInteritemSpacingForSecionAtIndex section: Int) -> CGFloat {
        return 50
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath) {
            cell.contentView.layer.borderWidth = 0.0
            cell.contentView.layer.shadowRadius = 0.0
            cell.contentView.layer.shadowOpacity = 0
        }
        
        if let indexPath = context.nextFocusedIndexPath,
            
            let cell = collectionView.cellForItem(at: indexPath) {
            
            
            cell.contentView.layer.borderWidth = 5.0
            cell.contentView.layer.borderColor = UIColor.purple.cgColor
            cell.contentView.layer.shadowColor = UIColor.purple.cgColor
            cell.contentView.layer.shadowRadius = 30.0
            cell.contentView.layer.shadowOpacity = 0.9
            cell.contentView.layer.shadowOffset = CGSize(width: 20, height: 20)
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
        }
    }
    
    func getCurrentDate(){
        
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        print("year:\nmonth:\nday:",year,month,day)
    }
    
    func getJson(){
        if let url = URL(string: jsonURL!) {
            if let data = try? Data(contentsOf: url) {
                let json = JSON(data: data)
                
                if json["status"]=="ok" {
                    // we're OK to parse!
                    service1StartTime = json["service1-starttime"].stringValue
                    service2StartTime = json["service2-starttime"].stringValue
                    wednesdayStartTime = json["wednesday-starttime"].stringValue
                    tcInfo = json["tcinfo"].stringValue
                    headerImageUrl = json["header-image-url"].stringValue
                    backgroundImageUrl = json["background-image-url"].stringValue
                    videoImageURL = json["video-image-view-url"].stringValue

                    parse(json: json)
                }
            }
        }
    }
    
    
    
}
