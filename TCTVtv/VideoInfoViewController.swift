//
//  VideoInfoViewController.swift
//  TCTV
//
//  Created by Michael on 12/7/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit
import AVKit


class VideoInfoViewController: UIViewController {
    
    var buttonImageURL:NSURL?
    var videoInfoText: NSString?
    var sermonTitleText: NSString?
    var selectedURL:URL!
    var avPlayer:AVPlayer?
    var isPlayButtonPressed:Bool?
    

    @IBOutlet weak var sermonTitleLabel: UILabel!
    @IBOutlet weak var videoInfoImageView: UIImageView!
    @IBOutlet weak var videoInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data = try? Data(contentsOf: self.buttonImageURL as! URL)
        
        self.videoInfoLabel.text = videoInfoText as String?
        
        self.videoInfoImageView.image = UIImage(data:data!)
        
        self.sermonTitleLabel.text = sermonTitleText as String?

        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        avPlayer?.pause()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClick(_ sender: UIButton) {
        avPlayer?.pause()
        isPlayButtonPressed = true
        
        performSegue(withIdentifier: "PlayVideoSegue", sender: self)
        
    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        
        if isPlayButtonPressed! {
            if segue.identifier == "PlayVideoSegue"{
                
                let destination = segue.destination as! AVPlayerViewController
                
                destination.player = AVPlayer(url: selectedURL as URL)
                destination.player?.volume = 1
                destination.player?.play()
                
            }
            isPlayButtonPressed = false
        }
        
        
    }

}
