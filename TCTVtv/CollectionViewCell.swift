//
//  CollectionViewCell.swift
//  TCTV
//
//  Created by Michael on 12/1/16.
//  Copyright © 2016 Michael Usry. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var serviceImageView: UIImageView!

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    private func commonInit()
    {
        // Initialization code
        
        self.layoutIfNeeded()
        self.layoutSubviews()
        self.setNeedsDisplay()
    }

  /*
     func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if (self.isFocused)
        {
            
            self.serviceImageView.adjustsImageWhenHighlighted = true

        }
        else
            
        {
            self.serviceImageButton.adjustsImageWhenHighlighted = false
        }
    }

 */
 
}
